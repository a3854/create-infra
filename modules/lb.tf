resource "aws_elb" "lb" {
  name               = "test-lb"
  availability_zones = ["us-west-2a"]

  listener {
    instance_port     = 8000
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
   }

   instances                   = [aws_instance.instance1.id, aws_instance.instance2.id]
   cross_zone_load_balancing   = true
   idle_timeout                = 400
   connection_draining         = true
   connection_draining_timeout = 400

   tags = {
     Name = "webserver-terraform-elb"
   }
}
