module "webserver" {
  source = "./modules"
  aws_secret_access_key = var.aws_secret_access_key
  aws_access_key_id = var.aws_access_key_id
}


terraform {
  backend "remote" {
    organization = "greatdev"

    workspaces {
      name = "create-infra"
    }
  }
}
