resource "aws_instance" "instance1" {
  key_name      = aws_key_pair.deployer.key_name
  ami           = "ami-03d5c68bab01f3496"
  instance_type = "t2.micro"
  tags          = {
    Name          = "webserver1"
  }

  vpc_security_group_ids = [
    aws_security_group.ubuntu.id
  ]

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("key")
    host        = self.public_ip
  }
}

resource "aws_instance" "instance2" {
  key_name      = aws_key_pair.deployer.key_name
  ami           = "ami-03d5c68bab01f3496"
  instance_type = "t2.micro"
  tags          = {
    Name          = "webserver2"
  }

  vpc_security_group_ids = [
    aws_security_group.ubuntu.id
  ]

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("key")
    host        = self.public_ip
  }
}
