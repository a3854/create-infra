# Creating Infrastructure with Terraform

Terraform is an IaC (Infrastructure as Code) used to deploy Infrastructure; detailed documentation may be found on The [Terraform Doc Page](https://www.terraform.io/docs/index.html).

## Installation

Depending on your OS, use [the Terraform CLI guide](https://www.terraform.io/downloads.html) to install on your machine, if necessary. On MacOS, run:

```bash
brew install terraform
```

## Usage

The state file has been configured against a remote backend. All the necessary changes to the Infrastructure may only be pushed via a custom branch and approved via a Pull Request:

```bash
# Add
git add <file(s)>

# Commit
git commit -m "Including a specific message - this may be useful on the PR"

# Push
git push (set upstream may be required)

# PR
Pull Request
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)